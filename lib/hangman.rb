class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(opt_hash = {})
    defaults = {
      guesser: HumanPlayer.new,
      referee: ComputerPlayer.new
    }

    opt_hash = defaults.merge(opt_hash)
    @guesser = opt_hash[:guesser]
    @referee = opt_hash[:referee]
    @board = "_"
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    # puts @referee.secret_word #Just using for testing
    @guesser.register_secret_length(secret_word_length)
    (secret_word_length-1).times {@board += "_"}
    puts @board
  end

  def take_turn
    alpha = ("a".."z").to_a.join("")
    letter = "" #So we can enter the until loop
    letter = @guesser.guess(@board) until alpha.include?(letter) && !@board.include?(letter)
    indices = @referee.check_guess(letter)
    update_board(indices, letter)
    @guesser.handle_response(letter, indices)
  end

  def update_board(indices,letter)
    indices.each {|ele| @board[ele] = letter}
    puts "Updated board: #{@board}"
  end

  def play
    setup

    until (@board.downcase == @referee.secret_word.downcase) || (!@board.include?("_"))
      take_turn
    end

    puts "Congrats, you won! The word was: #{@board}"
  end
end

class HumanPlayer
  attr_reader :secret_word, :secret_word_length

  def initialize

  end

  def pick_secret_word
    @secret_word = ""
    puts "Enter the word length for your secret word:"
    length = gets.chomp.to_i
  end

  def check_guess(letter)
    # indices = []
    # @secret_word.split("").each_with_index {|ele,idx| indices << idx if ele == letter.downcase}
    # puts "Correct! #{letter} appears at indice(s): #{indices}" if indices.length != 0
    # puts "Incorrect, #{letter} does not appear at any indices" if indices.length == 0
    # puts ""
    # indices

    puts "The computer guessed: #{letter}"
    indices = []
    puts "List all of the indices that include this letter. For example: 1,2,5"
    puts "Be sure there are not any spaces, and each number is separated by a comma."
    puts "Type 'none' if there the word doesn't contain this letter."
    input = gets.chomp
    return indices if input.downcase == "none"
    indices = input.split(",")
    indices.map! { |ele| ele.to_i }
    p indices
    indices

  end

  def register_secret_length(length)
    @secret_word_length = length
  end

  def guess(board = "")
    puts "Guess a letter:"
    letter = gets.chomp
  end

  # def handle_response
  #
  # end
end

class ComputerPlayer
  attr_reader :secret_word, :candidate_words, :secret_word_length

  def initialize(dictonary = File.readlines("lib/dictionary.txt").map {|word| word.chomp})
    @dictonary = dictonary
  end

  def pick_secret_word
    @secret_word = @dictonary[rand(@dictonary.length)]
    @secret_word_length = @secret_word.length
  end

  def check_guess(letter)
      indices = []
      @secret_word.split("").each_with_index {|ele,idx| indices << idx if ele == letter.downcase}
      puts "Correct! #{letter} appears at indice(s): #{indices}" if indices.length != 0
      puts "Incorrect, #{letter} does not appear at any indices" if indices.length == 0
      puts ""
      indices
  end

  def register_secret_length(length)
    @candidate_words = @dictonary.map { |word| word if word.length == length}
    @candidate_words.select! { |word| !word.nil?}
    @secret_word_length = length
  end

  def guess(board="")
    #Shouldn't need the random guess anymore, because we passed phase II and
    #we are ready to try III
    # alpha = ("a".."z").to_a
    # alpha[rand(alpha.length)]

    hash = Hash.new(0)
    @candidate_words.each { |word| word.each_char { |char| hash[char] += 1 if !board.include?(char)}}
    guess = hash.max_by {|k,v| v}[0]
    guess
  end

  def handle_response(char, arr)
    if arr.empty?
      @candidate_words.reject! {|word| word.include?(char)}
    else
      arr.each {|idx| @candidate_words.reject! {|word| word[idx] != char}}
      @candidate_words.reject! {|word| word.count(char) > arr.length}
    end
  end
end

# #Use this to test:
# defaults = {
#   guesser: ComputerPlayer.new,
#   referee: HumanPlayer.new
# }
# game = Hangman.new(defaults)
# game.play
